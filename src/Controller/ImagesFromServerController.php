<?php

namespace Drupal\ckeditor_imagesfromserver\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StreamWrapper\PublicStream;

/**
 * Controller routines for page ckeditor_imagesfromserver routes.
 */
class ImagesFromServerController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct() {}

  /**
   * Render a list of entries in the database.
   */
  public function importer() {
    $data = !empty($_POST) ? $_POST : NULL;

    if (empty($data)) {
      echo $this->generateErrorMessage('Invalid usage, no data posted.');
      die(0);
    }

    if (empty($data['externalSrc'])) {
      echo $this->generateErrorMessage('Invalid usage, missing externalSrc.');
      die(0);
    }

    $externalSrc = $data['externalSrc'] ;

    $basePath = PublicStream::basePath() . '/';
    $baseUrl = '/' . $basePath;

    $UA = $_SERVER['HTTP_USER_AGENT'];
    echo $this->generateJSON($basePath, $externalSrc, $UA, $baseUrl);
    exit;
  }

  /**
   *  Set of allowed extensions for images.
   */
  const AllowedExtensions = "jpe?g|gif|png|bmp";

  /**
   * Error message that aborted the import.
   */
  public $errorMessage;

  /**
   *  Path of the imported file (successfully).
   */
  public $importedFile;

  /**
   * {@inheritdoc}
   */
  private $ContentType;

  /**
   * {@inheritdoc}
   */
  private $detectedExtension;

  /**
   * {@inheritdoc}
   */
  public static function generateErrorMessage($msg) {
    return "{\"error\": \"" . self::toJSON($msg) . "\"}";
  }

  /**
   * {@inheritdoc}
   */
  public function generateJSON($folder, $image, $UA, $baseUrl) {
    $this->ProcessImportRequest($folder, $image, $UA);

    if ($this->errorMessage!="") {
      return self::generateErrorMessage($this->errorMessage);
    }

    if ($this->importedFile!="") {
      return "{\"newSrc\": \"" . self::toJSON($baseUrl . $this->importedFile) . "\"}";
    }

    return "{\"error\": \"Nothing has been processed\"}";
  }

  /**
   * {@inheritdoc}
   */
  private function DetectFileName($url) {
    if (!preg_match("/\/([^\?\/]+)(\?|$)/", $url, $matches)) {
      return "";
    }

    $name = $matches[1];

    $extension = pathinfo($name, PATHINFO_EXTENSION);
    if ($extension!="") {
      $extension = strtolower($extension);
      if (preg_match("/^" . self::AllowedExtensions . "$/", $extension)) {
        $this->detectedExtension = true;
      }
      else {
        $pos = strpos($name, ".");
        $name = substr($name, 0, $pos) . ".jpg";
      }
    }
    else {
      $name = $name.".jpg";
    }

    // Clean up filename.
    return self::CleanFilename($name);
  }

  /**
   * {@inheritdoc}
   */
  public function ProcessImportRequest($folder, $image, $UA) {
    $name = $this->DetectFileName($image);
    if ($name=="") {
      $name = "tmpFile.jpg";
    }

    $localPath = $folder . $name;
    // Create unique filename.
    $localPath = $this->CreateUniqueFilename($localPath);
    $name = pathinfo($localPath, PATHINFO_BASENAME);

    $this->downloadFile($image, $localPath);

    $status = $this->cURLdownload($image, $localPath, $UA);
    if ($status != "") {
      $this->setError($status);
      return;
    }

    // Check that it isn't an empty file
    $size = self::getFileSize($localPath);
    if ($size == 0) {
      $this->setError("The downloaded file was empty.");
      return;
    }

    if ((!$this->detectedExtension) && ($this->ContentType!="") ) {
      $newName = $this->ValidateExtension($this->ContentType, $localPath);
      if ($newName!="") {
        $name = $newName;
      }
    }

    $this->importedFile = $name;
  }

  /**
   * {@inheritdoc}
   */
  public function downloadFile($url, $path) {
    $newfname = $path;
    $file = fopen ($url, "rb");
    if (!empty($file)) {
      $newf = fopen ($newfname, "wb");

      if (!empty($newf)) {
        while (!feof($file)) {
          fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
        }
      }
    }

    if (!empty($file)) {
      fclose($file);
    }

    if (!empty($newf)) {
      fclose($newf);
    }
  }

  /**
   * {@inheritdoc}
   */
  private static function cURLcheckBasicFunctions() {
    if (!function_exists("curl_init") ||
      !function_exists("curl_setopt") ||
      !function_exists("curl_exec") ||
      !function_exists("curl_close") ) {

      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  private function cURLdownload($url, $file, $UA) {
    if ( !self::cURLcheckBasicFunctions() ) {
      $this->downloadFile($url, $file);
      return;
    }

    $ch = curl_init();
    if (!$ch) {
      return "FAIL: curl_init()";
    }

    $fp = fopen($file, "w");
    if (!$fp) {
      curl_close($ch); // to match curl_init()
      return "FAIL: fopen()";
    }

    if ( !curl_setopt($ch, CURLOPT_URL, $url) ) {
      fclose($fp); // to match fopen()
      curl_close($ch); // to match curl_init()
      return "FAIL: curl_setopt(CURLOPT_URL)";
    }

    // Skip SSL checks
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_USERAGENT, $UA);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($ch, CURLOPT_REFERER, 'http://domain.com/');

    if ( !curl_setopt($ch, CURLOPT_FILE, $fp) ) return "FAIL: curl_setopt(CURLOPT_FILE)";

    if ((!ini_get('open_basedir') && !ini_get('safe_mode')) ) {

    }
    else {
      if ( !curl_setopt($ch, CURLOPT_HEADER, 0)) return "FAIL: curl_setopt(CURLOPT_HEADER)";
      if ( !curl_setopt($ch, CURLOPT_FORBID_REUSE, true)) return "FAIL: curl_setopt(CURLOPT_FORBID_REUSE)";
    }

    $status = curl_exec($ch);
    if ( !$status ) {
      $HttpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      fclose($fp);
      return "FAIL: curl_exec()=" .$status . ", HttpCode=" . $HttpCode;
    }

    $this->ContentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

    curl_close($ch);
    fclose($fp);

    return;
  }

  /**
   * {@inheritdoc}
   */
  private function ValidateExtension($ContentType, $file) {
    if (substr($ContentType, 0, 6) != "image/") {
      unlink($file);
      $this->setError("Non image returned by the server");
      return;
    }

    $format = substr($ContentType, 6);
    // fixme: validate formats
    switch ($format) {
      case "jpeg":
        $format = "jpg";
        break;

      default:
        break;
    }

    // default filename is .jpg, rename if the content states otherwise
    if ($format != "jpg") {
      $parts = pathinfo($file);
      $folder = $parts['dirname'];
      $name = $parts['basename'];

      $format = self::CleanFilename($format);
      $newName = str_replace(".jpg", "." . $format, $name);
      $newLocalPath = $folder . DIRECTORY_SEPARATOR . $newName;
      // Create unique filename
      $newLocalPath = $this->CreateUniqueFilename($newLocalPath);

      rename($file, $newLocalPath);
      $name = pathinfo($newLocalPath, PATHINFO_BASENAME);

      return $name;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  private function setError($msg) {
    $this->errorMessage = $msg;
  }

  /**
   * {@inheritdoc}
   */
  private static function getFileSize($mappedPath) {
    if (!file_exists($mappedPath)) {
      return 0;
    }

    return filesize($mappedPath);
  }

  /**
   * {@inheritdoc}
   */
  private static function CreateUniqueFilename($path) {
    // If it doesn't exists, it's the good one. Use it!
    if (!file_exists($path)) {
      return $path;
    }

    $parts = pathinfo($path);
    $directory = $parts['dirname'];
    $filename = $parts['filename'];
    $extension = $parts['extension'];
    $counter = 1;

    $newFullPath = NULL;
    // Iterate until we find a good one.
    while (true) {
      $newFilename = $filename . "(" . $counter. ")." . $extension;
      $newFullPath = $directory . DIRECTORY_SEPARATOR . $newFilename;
      $counter++;
      if (!file_exists($newFullPath)) {
        break;
      }
    }

    return $newFullPath;
  }

  /**
   * {@inheritdoc}
   */
  private static function CleanFilename($name) {
    $replacement = "_";
    $t = $name;

    $t = str_replace("%20", "_", $t);

    // Remove everything that it's specifically allowed
    $t = preg_replace("/[^a-zA-Z\d\.\(\)\-_\/]/", $replacement, $t);

    if ($replacement != "") {
      // collapse them to avoid a long list of ___
      $t = preg_replace("/" . $replacement . "{2,}/", $replacement, $t);
    }

    // clean up ..
    $t = preg_replace("/\.{2,}/", ".", $t);

    return $t;
  }

  /**
   * {@inheritdoc}
   */
  private static function toJSON($input) {
    $input = str_replace("\\", "\\\\", $input);
    $input = str_replace("\"", "\\\"", $input);
    $input = str_replace("\r", "\\\r", $input);
    $input = str_replace("\n", "\\\n", $input);

    return $input;
  }

}
