<?php

namespace Drupal\ckeditor_imagesfromserver\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "ImagesFromServer Button" plugin.
 *
 * @CKEditorPlugin(
 *   id = "imagesfromserver",
 *   label = @Translation("ImagesFromServer Plugin")
 * )
 */
class ImagesFromServerPlugin extends CKEditorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'ImagesFromServer' => [
        'label' => $this->t('ImagesFromServer'),
        'image' => drupal_get_path('module', 'ckeditor_imagesfromserver') . '/icons/icon.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $plugin_path = $this->getLibraryPath() . '/plugin.js';
    if (file_exists($plugin_path)) {
      return $plugin_path;
    }
    return FALSE;
  }

  /**
   * Get library path.
   */
  public function getLibraryPath() {
    // See https://www.drupal.org/project/ckeditor_font/issues/3046772 for
    // considerations on $plugin_path moving forward, from ambiguity in 'font'.
    // Following the logic in Drupal 8.9.x and Drupal 9.x
    // ----------------------------------------------------------------------
    // Issue #3096648: Add support for third party libraries in site specific
    // and install profile specific libraries folders
    // https://www.drupal.org/project/drupal/issues/3096648
    //
    // https://git.drupalcode.org/project/drupal/commit/1edf15f
    // -----------------------------------------------------------------------
    // Search sites/<domain>/*.
    $directories[] = \Drupal::service('site.path') . '/libraries/';

    // Always search the root 'libraries' directory.
    $directories[] = 'libraries/';

    // Installation profiles can place libraries into a 'libraries' directory.
    if ($installProfile = \Drupal::installProfile()) {
      $profile_path = drupal_get_path('profile', $installProfile);
      $directories[] = "$profile_path/libraries/";
    }

    foreach ($directories as $dir) {
      if (file_exists(DRUPAL_ROOT . '/' . $dir . 'imagesfromserver/plugin.js')) {
        return $dir . 'imagesfromserver';
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = array();

    $account = \Drupal::service('current_user')->getAccount();

    if (AccessResult::allowedIfHasPermission($account, 'access ckeditor imagesfromserver')) {
      $config['imagesfromserver_importer'] = '/ckeditor_imagesfromserver/importer';
    }

    return $config;
  }

}
